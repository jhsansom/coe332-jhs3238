import csv

## PROBLEM 1
## Write a function that reads the csv file and prints out a list of dictionaries with the data


def read_csv(filename):
        with open(filename) as file:
                reader = csv.reader(file, delimiter=',')
                data_list = []
                id = -1
                for row in reader:
                        if id < 0:
                                id += 1
                                continue
                        row_dict = {
				'id': id,
				'year': int(row[0]),
				'rain': float(row[1]),	
				}
                        id += 1
                        data_list.append(row_dict)
                return data_list

## PROBLEM 2
## Write a function that returns dicitonary entries between start and end variables for year

def get_data_between_years(filename, start=1850,end=1979):
        data_list = read_csv(filename)
        year_list = []
        for entry in data_list:
                if entry['year'] >= start and entry['year'] <= end:
                        year_list.append(entry)
        return year_list


## PROBLEM 3

def get_limited_years(filename, limit=130, offset=0):
        data_list = read_csv(filename)
        indices = list(range(offset,130))
        too_long = (len(indices) > limit)
        while too_long:
                indices.pop()
                too_long = (len(indices) > limit)
        year_list = []
        for i in indices:
                year_list.append(data_list[i])
        return year_list


## PROBLEM 4

if __name__ == '__main__':
        print('Would you like to:')
        print('   (a) Print the entire data set?')
        print('   (b) Search by start and end year?')
        print('   (c) Search by limit and offset?')

        answer = raw_input('   Type a/b/c\n')

        filename = 'data.csv'
        if answer=='a':
                print(read_csv(filename))
        elif answer=='b':
                start = raw_input('Type a start year or press ENTER\n')
                end = raw_input('Type an end year or press ENTER\n')
                if start == '' and end == '':
                        print(get_data_between_years(filename))
                elif end == '':
                        print(get_data_between_years(filename, start=int(start)))
                elif start == '':
                        print(get_data_between_years(filename, end=int(end)))
                else:
                        print(get_data_between_years(filename, start=int(start), end=int(end)))
        elif answer=='c':	
                limit = raw_input('Type a limit or press ENTER\n')
                offset = raw_input('Type an offset or press ENTER\n')
                if limit == '' and offset == '':
                        print(get_limited_years(filename))
                elif offset == '':
                        print(get_limited_years(filename, offset=int(offset)))
                elif limit == '':
                        print(get_limited_years(filename, limit=int(limit)))
                else:
                        print(get_limited_years(filename, limit=int(limit), offset=int(offset)))


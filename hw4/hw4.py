# Homework 4
# Jake Sansom

import requests
from ast import literal_eval
import json

r = requests.get('http://localhost:5000/rain?start=1885&end=1884')
hello = json.loads(r.content)
print(hello)

def get_r():
    return requests.get('http://localhost:5000/rain')

def test_returns_200():
    r = get_r()
    assert r.status_code == 200

def test_returns_json():
    try:
        r = get_r()
        json_data = json.loads(r.content)
        assert True
    except:
        assert False

def test_decode_to_list():
    r = get_r()
    json_data = json.loads(r.content)
    assert type(json_data) == list

def test_list_of_dicts():
    r = get_r()
    json_data = json.loads(r.content)
    for i in json_data:
        assert type(i) == dict

def test_three_keys():
    r = get_r()
    json_data = json.loads(r.content)
    for i in json_data:
        assert len(i.keys()) == 3

def test_list_of_dicts():
    r = get_r()
    json_data = json.loads(r.content)
    for i in json_data:
        for j in i.keys():
            assert type(j) == str

def test_num_dicts():
    r = get_r()
    json_data = json.loads(r.content)
    assert len(json_data) == 130

def test_first():
    r = get_r()
    json_data = json.loads(r.content)
    assert json_data[0]['year'] == 1850
    assert json_data[0]['rain'] == 852
    assert json_data[0]['id'] == 0

def test_last():
    r = get_r()
    json_data = json.loads(r.content)
    assert json_data[len(json_data)-1]['year'] == 1979
    assert json_data[len(json_data)-1]['rain'] == 996
    assert json_data[len(json_data)-1]['id'] == 129

############################
# PART 2
############################

def test_start_end():
    r = requests.get('http://localhost:5000/rain?start=1875&end=1880')
    assert r.status_code == 200
    try:
        data = json.loads(r.content)
        assert type(data) == list
    except:
        assert False
    assert len(data) == 6

def test_start():
    r = requests.get('http://localhost:5000/rain?start=1975')
    assert r.status_code == 200
    try:
        data = json.loads(r.content)
        assert type(data) == list
    except:
        assert False
    assert len(data) == 5

def test_end():
    r = requests.get('http://localhost:5000/rain?end=1855')
    assert r.status_code == 200
    try:
        data = json.loads(r.content)
        assert type(data) == list
    except:
        assert False
    assert len(data) == 6

def test_start_greater():
    r = requests.get('http://localhost:5000/rain?start=1875&end=1874')
    assert r.status_code == 200
    try:
        data = json.loads(r.content)
        assert type(data) == list
    except:
        assert False
    assert len(data) == 0

def test_str_start():
    r = requests.get('http://localhost:5000/rain?start=abc')
    assert r.status_code == 400
    try:
        data = json.loads(r.content)
        assert True
    except:
        assert False

    assert type(data) == str



# HW 3
# Jake Sansom

import sys


from hw1 import read_csv, get_data_between_years, get_limited_years

from flask import *
import requests
import json

app = Flask(__name__)

# PROBLEM 1
file = 'data.csv'

@app.route('/rain', methods=['GET'])
def rain():
    start = request.args.get('start')
    end = request.args.get('end')
    limit = request.args.get('limit')
    offset = request.args.get('offset')

    if start:
        start = int(start)
    if end:
        end = int(end)
    if limit:
        limit = int(limit)
    if offset:
        offset = int(offset)

    if (start or end) and (limit or offset):
        return 'ERROR: cannot request start/end and limit/offset\n', 400
    
    if start and start < 1850:
        return 'ERROR: cannot request start less than 1850\n', 400
    if end and end > 1979:
        return 'ERROR: cannot request end greater than 1979\n', 400
    if limit and limit < 0:
        return 'ERROR: cannot request a negative limit\n', 400
    if offset and offset > 129:
        return 'ERROR: cannot request offset greater than 129\n', 400

    if start:
        if end:
            result = get_data_between_years(file, start=start, end=end)
        else:
            result = get_data_between_years(file, start=start)
    elif end: 
        result = get_data_between_years(file, end=end)
    elif limit:
        if offset:
            result = get_limited_years(file, limit=limit, offset=offset)
        else:
            result = get_limited_years(file, limit=limit)
    elif offset:
        result = get_limited_years(file, offset=offset)
    else:
        result = read_csv(file)

    return jsonify(result)


@app.route('/rain/<int:id>', methods=['GET'])
def get_id(id):
    data = read_csv(file)
    result = data[id]

    return jsonify(result)

@app.route('/rain/time_unit/<int:year>', methods=['GET'])
def get_year(year):
    data = read_csv(file)

    for item in data:
        if item['year'] == year:
            return jsonify(item)


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')


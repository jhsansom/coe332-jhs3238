import sys
sys.path.insert(0, '/home/ubuntu/coe332-jhs3238/hw1/')
from hw1 import read_csv, get_data_between_years, get_limited_years

f = get_data_between_years
start_year = 1850
end_year = 1979

def test_returns_list():
    assert type(f('data.csv')) == list

def test_returns_list_of_dicts():
    for i in f('data.csv'):
        assert type(i) == dict

def test_dict_has_three_keys():
    for i in f('data.csv'):
        num_keys = 0
        for key in i:
            num_keys += 1

        assert num_keys == 3

def test_type_of_keys():
    for i in f('data.csv'):
        for key in i:
            assert type(key) == str

def test_num_of_dicts():
    num_dicts = 0
    for i in f('data.csv'):
        num_dicts += 1

    assert num_dicts == 130

def test_num_of_dicts_start():
    for starting in range(start_year, end_year):
        num_dicts = 0
        for i in f('data.csv', start=starting):
            num_dicts += 1

        assert num_dicts == end_year - starting + 1

def test_num_of_dicts_end():
    for ending in range(start_year, end_year):
        num_dicts = 0
        for i in f('data.csv', end=ending):
            num_dicts += 1

        assert num_dicts == ending - start_year + 1

def test_num_of_dicts_both():
    for starting in range(start_year, end_year):
        for ending in range(start_year, end_year):
            num_dicts = 0
            read = f('data.csv', start=starting, end=ending)
            for i in read:
                num_dicts += 1

            if starting <= ending:
                assert num_dicts == ending - starting + 1
            else:
                assert num_dicts == 0

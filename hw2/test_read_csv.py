import sys
sys.path.insert(0, '/home/ubuntu/coe332-jhs3238/hw1/')
from hw1 import read_csv, get_data_between_years, get_limited_years

def test_read_csv_returns_list():
    assert type(read_csv('data.csv')) == list

def test_read_csv_returns_list_of_dictionaries():
    for i in read_csv('data.csv'):
        assert type(i) == dict

def test_dict_has_three_keys():
    for i in read_csv('data.csv'):
        num_keys = 0
        for key in i:
            num_keys += 1

        assert num_keys == 3

def test_type_of_keys():
    for i in read_csv('data.csv'):
        for key in i:
            assert type(key) == str

def test_num_of_dicts():
    num_dicts = 0
    for i in read_csv('data.csv'):
        num_dicts += 1

    assert num_dicts == 130

def test_first_and_last():
    for i in read_csv('data.csv'):
        if i['year'] == '1850':
            assert i['rain'] == 852
        elif i['year'] == '1979':
            assert i['rain'] == 996

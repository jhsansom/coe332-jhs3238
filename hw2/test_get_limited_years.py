import sys
sys.path.insert(0, '/home/ubuntu/coe332-jhs3238/hw1/')
from hw1 import read_csv, get_data_between_years, get_limited_years

f = get_limited_years

start_year = 1850
end_year = 1979
inrange = list(range(0, end_year - start_year + 1))

def test_returns_list():
    assert type(f('data.csv')) == list

def test_returns_list_of_dicts():
    for i in f('data.csv'):
        assert type(i) == dict

def test_dict_has_three_keys():
    for i in f('data.csv'):
        num_keys = 0
        for key in i:
            num_keys += 1

        assert num_keys == 3

def test_type_of_keys():
    for i in f('data.csv'):
        for key in i:
            assert type(key) == str

def test_num_of_dicts():
    num_dicts = 0
    for i in f('data.csv'):
        num_dicts += 1

    assert num_dicts == 130

def test_num_dicts_limit():
    for limiting in inrange:
        num_dicts = 0
        for i in f('data.csv', limit=limiting):
            num_dicts += 1

        assert num_dicts == limiting

def test_num_dicts_offset():
    for offsetting in inrange:
        num_dicts = 0
        for i in f('data.csv', offset=offsetting):
            num_dicts += 1

        assert num_dicts == end_year - start_year + 1 - offsetting

def test_both():
    for limiting in inrange:
        for offsetting in inrange:
            num_dicts = 0
            for i in f('data.csv', limit=limiting, offset=offsetting):
                num_dicts += 1

            if limiting <= (end_year - start_year + 1 - offsetting):
                assert num_dicts == limiting
            else:
                assert num_dicts == end_year - start_year + 1 - offsetting




